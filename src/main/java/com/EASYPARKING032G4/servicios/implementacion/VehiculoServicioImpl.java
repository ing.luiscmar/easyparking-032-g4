package com.EASYPARKING032G4.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;

import com.EASYPARKING032G4.daos.VehiculoDao;
import com.EASYPARKING032G4.modelos.Vehiculos;
import com.EASYPARKING032G4.servicios.ServicioVehiculo;

public class VehiculoServicioImpl implements ServicioVehiculo {

    @Autowired

    VehiculoDao vehiculoDao;

    public Vehiculos save(Vehiculos vehiculo) {

        Vehiculos nuevoVehiculo = vehiculoDao.save(new Vehiculos(vehiculo.getTipoVehiculo(), vehiculo.getPlaca(),
                vehiculo.getFecha(), vehiculo.getHora(), vehiculo.getHoraSalida()));

        return nuevoVehiculo;

    }

}
