package com.EASYPARKING032G4.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EASYPARKING032G4.modelos.Usuarios;

public interface RolesDao extends JpaRepository<Usuarios, Long> {

    public Usuarios findByNombreUsuario(String nombreRol);

}