package com.EASYPARKING032G4.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EASYPARKING032G4.modelos.Vehiculos;

public interface VehiculoDao extends JpaRepository<Vehiculos, Long> {

}