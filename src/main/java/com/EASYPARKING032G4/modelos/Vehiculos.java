package com.EASYPARKING032G4.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculos")
public class Vehiculos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idVehiculo;

    @Column(name = "tipoVehiculo", nullable = false)
    private String tipoVehiculo;

    @Column(name = "placa", nullable = false)
    private String placa;

    @Column(name = "fecha", nullable = false)
    private String fecha;

    @Column(name = "hora", nullable = false)
    private String hora;

    @Column(name = "horaSalida", nullable = false)
    private String horaSalida;

    public Vehiculos() {
    }

    public Vehiculos(String tipoVehiculo, String placa, String fecha, String hora, String horaSalida) {
        this.tipoVehiculo = tipoVehiculo;
        this.placa = placa;
        this.fecha = fecha;
        this.hora = hora;
        this.hora = horaSalida;
    }

    public long getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(long idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    @Override
    public String toString() {
        return "Vehiculos [fecha=" + fecha + ", hora=" + hora + ", horaSalida=" + horaSalida + ", idVehiculo="
                + idVehiculo + ", placa=" + placa + ", tipoVehiculo=" + tipoVehiculo + "]";
    }

}
