package com.EASYPARKING032G4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Easyparking032G4Application {

	public static void main(String[] args) {
		SpringApplication.run(Easyparking032G4Application.class, args);
	}

}
