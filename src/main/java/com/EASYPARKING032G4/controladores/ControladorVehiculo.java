package com.EASYPARKING032G4.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.EASYPARKING032G4.daos.VehiculoDao;
import com.EASYPARKING032G4.modelos.Vehiculos;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class ControladorVehiculo {

    @Autowired
    VehiculoDao vehiculoDao;

    @PostMapping("/vehiculos")
    public ResponseEntity<Vehiculos> guardarVehiculo(@RequestBody Vehiculos vehiculo) {
        Vehiculos nuevoVehiculo = new Vehiculos(vehiculo.getTipoVehiculo(), vehiculo.getPlaca(),
                vehiculo.getFecha(), vehiculo.getHora(), vehiculo.getHoraSalida());
        return new ResponseEntity<>(vehiculoDao.save(nuevoVehiculo), HttpStatus.CREATED);
    }

    @PutMapping("/vehiculos/{id}")
    public ResponseEntity<Vehiculos> actualizarVehiculos(@PathVariable long id, @RequestBody Vehiculos vehiculo) {
        Optional<Vehiculos> _vehiculo = vehiculoDao.findById(id);
        if (_vehiculo.isPresent()) {
            Vehiculos vehiculoActualizar = _vehiculo.get();
            vehiculoActualizar.setTipoVehiculo(vehiculo.getTipoVehiculo());
            vehiculoActualizar.setPlaca(vehiculo.getPlaca());
            vehiculoActualizar.setFecha(vehiculo.getFecha());
            vehiculoActualizar.setHora(vehiculo.getHora());
            vehiculoActualizar.setHoraSalida(vehiculo.getHoraSalida());
            return new ResponseEntity<>(vehiculoDao.save(vehiculoActualizar), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/vehiculos")
    public ResponseEntity<List<Vehiculos>> listarVehiculos() {

        List<Vehiculos> vehiculos = new ArrayList<Vehiculos>();

        vehiculoDao.findAll().forEach(vehiculos::add);

        if (vehiculos.isEmpty()) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {

            return new ResponseEntity<>(vehiculos, HttpStatus.OK);
        }

    }

    @GetMapping("/vehiculos/{id}")
    public ResponseEntity<Vehiculos> verVehiculo(@PathVariable Long id) {

        Optional<Vehiculos> vehiculo = vehiculoDao.findById(id);
        if (vehiculo.isPresent()) {
            return new ResponseEntity<>(vehiculo.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new Vehiculos(), HttpStatus.OK);
        }

    }

    @DeleteMapping("/vehiculos/{id}")
    public ResponseEntity<HttpStatus> borrarVehiculo(@PathVariable Long id) {

        vehiculoDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

}
